const app = getApp()

let request = (_url, _type, _data, callback,failcallback) => {
    var user_token = wx.getStorageSync('userToken');
    console.log("api:token:",user_token)
  // 获取本地保存的信息 
  wx.request({
    url: app.globalData.host + '/api' + _url,
    data: _data,
    method: _type,
    header: {
      'content-type': 'application/json',
      'appid': app.globalData.appid,
      'token': user_token
    },
    success: ((res) => {
      if (res.statusCode == 200 || res.statusCode == 201) {
        callback(res)
      }else if(res.statusCode === 401) {
        console.log("api request fail")
      }
    }),
    fail: ((res) => {
      failcallback(res)
    })
  })
}

module.exports = {
  request: request
}