const app = getApp()
const api = require('../../../util/api.js')

Page({
  onShareAppMessage() {
    return {
      title: '获取用户信息',
      path: 'packageAPI/pages/get-user-info/get-user-info'
    }
  },

  data: {
    hasUserInfo: false
  },
  getUserInfo(info) { // 2021.04微信官方已废弃此方法请使用 wx.getUserProfile
    const userInfo = info.detail.userInfo
    this.setData({
      userInfo,
      hasUserInfo: true
    })
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
  // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    let that = this;
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        var userToken = wx.getStorageSync('userToken');   //token是代替openid进行网络传输在前后端识别用户身份的标识，包含用户身份的接口都需要传token参数
        var data = {
        token: userToken,
        encryptedData: res.encryptedData,
        iv: res.iv,
        rawData: res.rawData,
        signature: res.signature
        };
        api.request('/miniapplogin/decryptInfo', 'POST', data, (response) => {         //decrypt接口只在前后端登录态都有效时可以调用，后台解密获得全部用户信息，并保存到用户帐号
        wx.setStorageSync('USER_INFO', response.data.data);              //decrypt接口只返回用户信息和刷新用户登录态无关
        that.setData({
          hasUserInfo: true,
          userInfo: response.data.data
        })
        app.globalData.hasUserInfo = true
        app.globalData.userInfo = response.data.data
        if(!(app.globalData.userInfo||'')){       //如果通过后台获取到的值是空的，就用前端获取的公开数据，注意前端的nickName和后台nickname大小写不一致
          console.log('使用前端公开用户数据')
          app.globalData.userInfo = res.userInfo
          that.setData({
            userInfo: res.userInfo
          })
        }
        });
      }
    })
  },
  clear() {
    this.setData({
      hasUserInfo: false,
      userInfo: {}
    })
  }
})
