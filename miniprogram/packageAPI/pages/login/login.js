const app = getApp()
const api = require('../../../util/api.js')

Page({
  onShareAppMessage() {
    return {
      title: '微信登录',
      path: 'package/API/pages/login/login'
    }
  },

  onLoad() {
    this.setData({
      hasLogin: app.globalData.hasLogin
    })
  },
  data: {},
  login() {
    const that = this
    //每次前端wx.login都会刷新code，即刷新session_key，因此对应后台 /miniapplogin/wxlogin 每次都返回新token
    wx.login({
      success: function (ret) {
        var code = ret.code;
        var data = { "code": code ,"appid": app.globalData.APPID };
        api.request('/miniapplogin/wxlogin', 'POST', data, (res) => {
          console.log(res);
          wx.setStorageSync('userToken', res.data.data.token);
          console.log(' userToken Success! ');
        });
      }
    })
  }
})
