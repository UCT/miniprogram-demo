const app = getApp()
const api = require('../../util/api.js')

Page({
  onShow() {
    wx.reportAnalytics('enter_home_programmatically', {})
  },
  onShareAppMessage() {
    return {
      title: '首页',
      path: 'page/component/index'
    }
  },

  data: {
    list: [
       {
        id: 'view',
        name: '视图容器',
        open: false,
        pages: ['view', 'scroll-view', 'swiper', 'movable-view', 'cover-view']
      }, {
        id: 'content',
        name: '基础内容',
        open: false,
        pages: ['text', 'icon', 'progress', 'rich-text']
      }, {
        id: 'form',
        name: '表单组件',
        open: false,
        pages: ['button', 'checkbox', 'form', 'input', 'label', 'picker', 'picker-view', 'radio', 'slider', 'switch', 'textarea', 'editor']
      }, {
        id: 'nav',
        name: '导航',
        open: false,
        pages: ['navigator']
      }, {
        id: 'media',
        name: '媒体组件',
        open: false,
        pages: ['image', 'video', 'camera', 'live-pusher', 'live-player']
      }, {
        id: 'map',
        name: '地图',
        open: false,
        pages: ['map']
      }, {
        id: 'canvas',
        name: '画布',
        open: false,
        pages: ['canvas', 'canvas-2d', 'webgl']
      }, {
        id: 'open',
        name: '微用户帐号',
        open: true,
        pages: ['ad', 'open-data', 'web-view']
      }, {
        id: 'obstacle-free',
        name: '无障碍访问',
        open: false,
        pages: ['aria-component']
      }
    ],
    theme: 'light'
  },

  onLoad(query) {
    // scene 需要使用 decodeURIComponent 才能获取到生成二维码时传入的 scene
    const scene = decodeURIComponent(query.scene)
    wx.checkSession({      
      success: function () { //session_key 未过期，并且在本生命周期一直有效。session_key是微信维护的登录态
        //检查第三方平台服务器端自定义登录态是否过期，过期就自动登录。与session_key一一对应
        var that = this;
        var userToken = wx.getStorageSync('userToken');
        api.request('/miniapplogin/checktoken', 'POST', {
          token: userToken
        }, (res) => {
          var checkStatus = res.data.data.check;
          //如果校验失败,第三方平台服务器端无有效自定义登录态
          if (!checkStatus) {
            wx.login({
              success: function (ret) {
                var code = ret.code;
                var data = { "code": code ,"appid": app.globalData.APPID };
                api.request('/miniapplogin/wxlogin', 'POST', data, (res) => {
                  wx.setStorageSync('userToken', res.data.data.token);
                });
              }
            })
          }
        });
      },
      fail: function () {
        // session_key 已经失效，需要重新执行登录流程
        //前端登录态失效，无论服务器端登录态如何，都重新登录并刷新前后端的自定义登录态token
        wx.login({
          success: function (ret) {
            var code = ret.code;
            var data = { "code": code ,"appid": app.globalData.APPID };
            api.request('/miniapplogin/wxlogin', 'POST', data, (res) => {
              wx.setStorageSync('userToken', res.data.data.token);
            });
          }
        })
      }
    })

    this.setData({
      theme: wx.getSystemInfoSync().theme || 'light'
    })

    if (wx.onThemeChange) {
      wx.onThemeChange(({ theme }) => {
        this.setData({ theme })
      })
    }

    
  },

  kindToggle(e) {
    const id = e.currentTarget.id
    const list = this.data.list
    for (let i = 0, len = list.length; i < len; ++i) {
      if (list[i].id === id) {
        list[i].open = !list[i].open
      } else {
        list[i].open = false
      }
    }
    this.setData({
      list
    })
    wx.reportAnalytics('click_view_programmatically', {})
  }
})
